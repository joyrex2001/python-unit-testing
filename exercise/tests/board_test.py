import unittest

from tictactoe import board

class Test_Board(unittest.TestCase):

    def test_Update(self):
        b = board.Board()
        ## TODO: implement test
        ## * check if position 0,0 contains board.E
        ## * insert X on position 0,0
        ## * check if position 0,0 contains board.X

    def test_Winner(self):
        ## TODO: fix this test
        b = board.Board()
        self.assertTrue(b.has_winner())

    def test_WinnerMoves(self):
        tests = [ 
            {
                "winner": board.X,
                "moves": [ [0,0,board.X], [0,1,board.X], [0,2,board.X] ]
            },
            {
                "winner": board.X,
                "moves": [ [0,0,board.X], [1,1,board.X], [2,2,board.X] ]
            },
            {
                "winner": board.X,
                "moves": [ [0,0,board.X], [1,0,board.X], [2,0,board.X] ]
            },
            {
                "winner": board.X,
                "moves": [ [0,2,board.X], [1,1,board.X], [2,0,board.X] ]
            },
        ]

        for test in tests:
            b = board.Board()
            for move in test["moves"]:
                b.put(*move)
            self.assertTrue(b.has_winner())
            self.assertEqual(b.get_winner(), test["winner"])


if __name__ == '__main__':
    unittest.main()