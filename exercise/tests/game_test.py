import unittest

from tictactoe import game

class Test_Board(unittest.TestCase):

    def test_Move(self):
        tests = [ 
            [ "0,2",  [0,2] ],
            [ "0, 2", [0,2] ],
        ]
        for test in tests:
            res = game.get_move_coord(test[0])
            self.assertEqual(list(res), test[1])

    def test_IsValidMove(self):
        tests = [ 
            [ "0,2",    True  ],
            [ " 0, 2 ", True  ],
            [ "a2 ",    False ],
            [ "0,2,1",  False ],
        ]
        for test in tests:
            res = game.is_valid_move(test[0])
            self.assertEqual(bool(res), test[1])


if __name__ == '__main__':
    unittest.main()