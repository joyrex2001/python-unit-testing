import random 
import re

from tictactoe import board

def is_valid_move(move):
    return re.match("^\\s*[012]\\s*,\\s*[012]\\s*$", move)

def get_move_coord(move):
    return map(lambda x: int(x), move.split(","))

def get_computer_coord(b):
    max = 25
    while max > 0:
        x = random.randint(0,2)
        y = random.randint(0,2)
        if b.get(x,y) == board.E:
            return [x,y]
        max = max-1
    return [1,1]

def Main():
    b = board.Board()
    print ("You are X, computer plays as O\n")
    print(b.to_string())

    player = board.X
    while not b.has_winner() and b.can_move():
        if player == board.X:
            move = input("your move (x,y)? ")
            if not is_valid_move(move):
                print ("invalid move entered...\n")
                continue
            c = get_move_coord(move)
        else:
            c = get_computer_coord(b)

        b.put(*c, player)

        print()
        print(b.to_string())

        if player == board.X:
            player = board.O
        else:
            player = board.X

    if not b.has_winner():
        print("Draw game...\n")
    else:
        winner = b.get_winner()
        print(f"Player {winner} won!\n")

    print ("Bye!\n")