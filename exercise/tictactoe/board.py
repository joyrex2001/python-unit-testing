E = "."
X = "X"
O = "O"

class Board:

    def __init__(self):
        self.board = [
            [ E, E, E ],
            [ E, E, E ],
            [ E, E, E ]
        ]

    def put(self,x,y,p):
        self.board[y][x] = p

    def get(self,x,y):
        if x < 0 or x > 2:
            return E
        if y < 0 or y > 2:
            return E
        return self.board[y][x]

    def can_move(self):
        for x in range(3):
            for y in range(3):
                if self.get(x,y) == E:
                    return True
        return False

    def has_winner(self):
        return self.get_winner() != E

    def get_winner(self):
        for x in range(3):
            for y in range(3):
                p = self.get(x,y)
                if p == E:
                    continue
                if p == self.get(x-1,y) == self.get(x+1,y):
                    return p
                if p == self.get(x,y-1) == self.get(x,y+1):
                    return p
                if p == self.get(x-1,y-1) == self.get(x+1,y+1):
                    return p
                if p == self.get(x-1,y+1) == self.get(x+1,y-1):
                    return p
        return E

    def to_string(self):
        res = []
        for row in self.board: 
            res.append( " " + " | ".join(row) + "\n" )
        return "---+---+---\n".join(res)
