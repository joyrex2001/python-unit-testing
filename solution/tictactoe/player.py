import re

from tictactoe import board

class Player:

    def __init__(self,p=board.X):
        self.piece = p

    def is_valid_move(self,move):
        return re.match("^\\s*[012]\\s*,\\s*[012]\\s*$", move)

    def move_to_coord(self,move):
        return map(lambda x: int(x), move.split(","))

    def get_move_coord(self,b):
        valid = False
        while not valid:
            move = input("your move (x,y)? ")
            if not self.is_valid_move(move):
                print ("invalid move entered...\n")
            else:
                valid = True
        return self.move_to_coord(move)