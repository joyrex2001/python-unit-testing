from tictactoe import board, computer, player

def play(b, players):
    turn = 0
    while not b.has_winner() and b.can_move():
        c = players[turn].get_move_coord(b)
        b.put(*c, players[turn].piece)
        print()
        print(b.to_string())
        turn ^= 1
    return b.get_winner()

def Main():
    b = board.Board()

    print ("You are X, computer plays as O\n")
    print(b.to_string())
    
    players = [ 
        player.Player(board.X),
        computer.Computer(board.O)
    ]

    winner = play(b, players)

    if not b.has_winner():
        print("Draw game...\n")
    else:
        print(f"Player {winner} won!\n")

    print ("Bye!\n")