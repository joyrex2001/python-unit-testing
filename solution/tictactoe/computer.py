import random

from tictactoe import board

class Computer:

    def __init__(self,p=board.O):
        self.piece = p
    
    def get_move_coord(self,b):
        max = 25
        while max > 0:
            x = random.randint(0,2)
            y = random.randint(0,2)
            if b.get(x,y) == board.E:
                return [x,y]
            max = max-1
        return [1,1]
