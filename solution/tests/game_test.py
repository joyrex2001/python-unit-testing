import unittest
from unittest import mock

from tictactoe import game, computer, board

class Test_Game(unittest.TestCase):

    def test_NaivePlay(self):
        b = board.Board()
        players = [ 
            computer.Computer(board.O),
            computer.Computer(board.O)
        ]
        winner = game.play(b, players)
        self.assertEqual(winner, board.O)

    def test_MockPlay(self):
        players = [ 
            computer.Computer(board.O),
            computer.Computer(board.O)
        ]

        with mock.patch.object(board.Board, 'has_winner', return_value=True):
            b = board.Board()
            winner = game.play(b, players)
            self.assertEqual(winner, board.E)

        with mock.patch.object(board.Board, 'has_winner', return_value=True):
            with mock.patch.object(board.Board, 'get_winner', return_value=board.X):
                b = board.Board()
                winner = game.play(b, players)
                self.assertEqual(winner, board.X)


if __name__ == '__main__':
    unittest.main()