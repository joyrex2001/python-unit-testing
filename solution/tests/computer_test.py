import unittest

from tictactoe import board, computer

class Test_Computer(unittest.TestCase):

    def test_GetMoveCoord(self):
        c = computer.Computer()
        b = board.Board()

        ## moves can not be identical
        res1 = c.get_move_coord(b)
        b.put(*res1, board.X)
        res2 = c.get_move_coord(b)
        self.assertNotEqual(list(res1),list(res2))

        ## full board
        for x in range(3):
            for y in range(3):
                b.put(x,y,board.X)
        resf = c.get_move_coord(b)
        self.assertEqual(list(resf), [1,1])


if __name__ == '__main__':
    unittest.main()